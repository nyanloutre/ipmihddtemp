import math
import subprocess
import time
from pySMART import DeviceList

MIN_FAN_SPEED = 30
MAX_FAN_SPEED = 100

MIN_HDD_TEMP = 30
MAX_HDD_TEMP = 50

devlist = DeviceList()

# Put fans in full speed mode
subprocess.run(["ipmitool", "raw", "0x30", "0x45", "0x01", "0x01"])

def main():
    while True:
        for device in devlist:
            device.update()

        # Linear fan speed between MIN_FAN_SPEED and MAX_FAN_SPEED
        fan_speed = max(
            min(
                math.ceil(
                    MIN_FAN_SPEED
                    + (
                        (MAX_FAN_SPEED - MIN_FAN_SPEED)
                        * (
                            (max([x.temperature for x in devlist]) - MIN_HDD_TEMP)
                            / (MAX_HDD_TEMP - MIN_HDD_TEMP)
                        )
                    )
                ),
                MAX_FAN_SPEED,
            ),
            MIN_FAN_SPEED,
        )

        subprocess.run(["ipmitool", "raw", "0x30", "0x70", "0x66", "0x01", "0x00", hex(fan_speed)])

        time.sleep(10)
