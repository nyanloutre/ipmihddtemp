from setuptools import setup

setup(
    name='ipmihddtemp',
    py_modules=['ipmihddtemp'],
    version='1.0',
    description='IPMI HDD temp fan control',
    author='Paul TREHIOU',
    author_email='paul@nyanlout.re',
    url='https://gitea.nyanlout.re/nyanloutre/ipmihddtemp',
    license='MIT',
    entry_points={
        'console_scripts': [
            'ipmihddtemp=ipmihddtemp:main'
        ]
    },
    install_requires=[
          'pysmart'
    ]
)
