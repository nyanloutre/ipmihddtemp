{
  description = "IPMI HDD temp fan control";

  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    python_pkgs = nixpkgs.legacyPackages.${system}.python3.pkgs;
    packageName = "ipmihddtemp";
    app = python_pkgs.buildPythonApplication rec {
      pname = packageName;

      version = builtins.substring 0 8 self.lastModifiedDate;

      src = ./.;

      postPatch = ''
        substituteInPlace ipmihddtemp.py \
          --replace "ipmitool" "${pkgs.ipmitool}/bin/ipmitool"
      '';

      doCheck = false;

      propagatedBuildInputs = with python_pkgs; [
        pysmart
      ];
    };
    module = ({ config, lib, pkgs, ... }: let
      cfg = config.${packageName};
    in {
      options.${packageName} = with lib.options; {
        enable = mkEnableOption packageName;
      };

      config = lib.mkIf cfg.enable {
        systemd.services.ipmihddtemp = {
          wantedBy = [ "multi-user.target" ];
          script = "${app}/bin/ipmihddtemp";
          enable = true;
        };
      };
    });
  in {
    defaultPackage.${system} = app;
    nixosModules.${packageName} = module;
    nixosModule = self.nixosModules.${packageName};
  };
}
